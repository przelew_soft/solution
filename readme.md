Welcome

I would like to describe a little bit this solution:


**Assumptions:**

1) Create MODERN, ROBUST easy to read and analyze solution

2) Use asynchronous technology

3) It wasn't mentioned in description file but - no external libraries 

**Description:**

There are 2 main Threads:
first is reading the config file resources/jobs.properties, and second one is aplying read jobs into the SchedulerExecutorService.
That SchedulerExecutorService is confugured for number of threads by NUMBER_OF_CRONJOBS variable. 

Data pass between 2 main Threads is done by ConcurrentHashMap which is designed to work with multiple Threads. I know it can be configured better, 
because now it is set by default to work with 16 Threads.


Second Thread is responsible for invoking CompletableFutures. I choose this technology because of No 1 on assumptions list AND it is asynchronous.
Which mean it is not blocking. Syntax is in Java 8 Lambdas and with method ".exceptionally()" you can easily pass exception up in hierarchy, and stop 
all other threads - which was one of your need.

**In serious software development I would recommend 2 approaches:**

1) Longer but state of art - Akka 

2) Easier - JavaRX, Spring Batch with admin panel

Testing multithreading is not my best site, so please have mercy. You wrote aboute UnitTest which are the most granulated, so it would be testing just methods.

For proper testing we need to create Integration Tests, wchich involves frameworks designed for MultiThreading testing.
<br>

I tried to use as much Java 8 as possible. Using Java 7 syntax nowadays, is not serious.


Jobs

- Addition - Will not throw exception

- Division - Will throw exception



Scheduling Works - (CompletableFuture) job will be run in the future


**What Can be better:**

1) AsynchTaskRunner.run  - there is switch/case state, and in real life it shouldn't be added manually. It can be dynamically generated from custom Annotations or Reflection with Naming convention

2) Test - this software should be much more tested with mocks

3) There is a problem with ScheduledThredPool - because even when Thread pool is set to few threads in constructor - it runs all at once ....

I could fix it all, but probably you don't want to wait that much time.

</pre>