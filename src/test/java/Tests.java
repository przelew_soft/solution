import jobs.impl.DivisionJobImpl;
import org.junit.Test;

/**
 * Created by przelew on 2017-06-19.
 */
public class Tests {

    @Test(expected = Exception.class)
    public void testExceptionThrown()
    {
        DivisionJobImpl division = new DivisionJobImpl();
        division.execute();
    }
}
