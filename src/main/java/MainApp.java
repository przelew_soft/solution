import DTO.JobDTO;

import java.util.UUID;
import java.util.concurrent.*;

/**
 * Created by przelew on 2017-06-18.
 */
public class MainApp {

    public static final int NUMBER_OF_CRONJOBS = 2;
    public static final int ASYNC_TASK_RUNNER_DELAY = 8;
    public static final int CONFIG_READER_DELAY = 8;

    public static void main(String[] args) throws ExecutionException, InterruptedException {



        ConcurrentHashMap<UUID, JobDTO> concurrentHashMap = new ConcurrentHashMap<>();
        ScheduledExecutorService jobsExecutorService = Executors.newScheduledThreadPool(NUMBER_OF_CRONJOBS);


        Executors.newSingleThreadScheduledExecutor()
                .scheduleWithFixedDelay(new ConfigReaderJob(concurrentHashMap) , 2, CONFIG_READER_DELAY, TimeUnit.SECONDS);


        Executors.newSingleThreadScheduledExecutor()
                .scheduleWithFixedDelay(new AsynchTaskRunner(concurrentHashMap, jobsExecutorService), 0, ASYNC_TASK_RUNNER_DELAY, TimeUnit.SECONDS);


    }


}
