import DTO.JobDTO;
import jobs.IJob;
import jobs.impl.AdditionJobImpl;
import jobs.impl.DivisionJobImpl;
import jobs.impl.MultiplicationJobImpl;
import jobs.impl.SubstractionJobImpl;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.UUID;
import java.util.concurrent.*;
import java.util.function.Supplier;

/**
 * Created by przelew on 2017-06-21.
 */
public class AsynchTaskRunner implements Runnable {

    private final ConcurrentHashMap<UUID, JobDTO> concurrentHashMap;
    private final ScheduledExecutorService jobsExecutorService;
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");

    public AsynchTaskRunner(final ConcurrentHashMap<UUID, JobDTO> concurrentHashMap, final ScheduledExecutorService jobsExecutorService) {

        this.concurrentHashMap = concurrentHashMap;
        this.jobsExecutorService = jobsExecutorService;
    }

    @Override
    public void run() {
        System.out.println("AsyncTaskRunner.run() - managing threads");

        concurrentHashMap.keySet().stream().forEach(uuid -> {

            JobDTO jobDTO = concurrentHashMap.get(uuid);

            long secondsToRun = jobDTO.isStartNow() ? 0 : verifyTimeToSchedule(jobDTO);


            switch (jobDTO.getName()) {
                case ("add"):

                    createCompletableFuture(new AdditionJobImpl(), secondsToRun);
                    break;

                case ("mul"):

                    createCompletableFuture(new MultiplicationJobImpl(), secondsToRun );
                    break;
                case ("div"):

                    createCompletableFuture(new DivisionJobImpl(),secondsToRun);
                    break;
                case ("sub"):

                    createCompletableFuture(new SubstractionJobImpl(),secondsToRun);
                    break;
            }

            /**
             * Needed to clear map from loaded task
             */
            concurrentHashMap.remove(uuid);

            });
    }


    private long verifyTimeToSchedule(JobDTO jobDTO) {
        LocalDateTime fromDateTime = LocalDateTime.now();
        LocalDateTime toDateTime = LocalDateTime.parse(jobDTO.getDateToRun(), formatter);

        return fromDateTime.until( toDateTime, ChronoUnit.SECONDS);

    }

    private void createCompletableFuture(Supplier<Void> iJob, long secondsToRun) {
        jobsExecutorService.schedule(() -> CompletableFuture.supplyAsync(iJob)
                .thenApply(aVoid -> {
                    /**
                     * Part for doing calculation after CompletableFuture has finished calculations.
                     */
                    return null;
                })
                .exceptionally(throwable -> {

                    /**
                     * Place where Executor service stops all threads, because of error escalated from CompletableFuture
                     */
                    jobsExecutorService.shutdownNow();
                    throwable.printStackTrace();

                    return null;

                }), secondsToRun, TimeUnit.SECONDS );




    }

}
