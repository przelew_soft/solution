package jobs.impl;

import jobs.IJob;

import java.util.function.Supplier;

/**
 * Created by przelew on 2017-06-18.
 */
public class SubstractionJobImpl implements Supplier<Void>, IJob {
    @Override
    public void execute() {

    }

    @Override
    public Void get() {
        execute();

        return null;
    }
}
