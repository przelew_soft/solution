package jobs.impl;

import jobs.IJob;

import java.util.Date;
import java.util.Random;
import java.util.function.Supplier;

/**
 * Created by przelew on 2017-06-18.
 */
public class AdditionJobImpl implements Supplier<Void>, IJob {

    @Override
    public Void get() {
        execute();

        return null;
    }


    @Override
    public void execute() {
        System.out.println("Additional thread run. "+ new Date());
        try {
            Thread.sleep(new Random().nextInt(2000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("\t\t\t THREAD: AdditionJobImpl: 2+2=4 "+ new Date());
    }
}
