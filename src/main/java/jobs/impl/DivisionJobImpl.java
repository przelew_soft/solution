package jobs.impl;

import jobs.IJob;

import java.util.function.Supplier;

/**
 * Created by przelew on 2017-06-18.
 */
public class DivisionJobImpl implements Supplier<Void>, IJob {

    @Override
    public void execute(){
        System.out.println("AdditionJobImpl from Run Thread.");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("\t\t\t AdditionJobImpl exiting Thread.");

        throw new NullPointerException("COS POSZLO NIE TAK");

    }

    @Override
    public Void get() {
        execute();

        return null;
    }
}
