import DTO.JobDTO;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

/**
 * Created by przelew on 2017-06-18.
 */
public class ConfigReaderJob implements Runnable{


    private ConcurrentHashMap<UUID, JobDTO> concurrentHashMap;

    public ConfigReaderJob(ConcurrentHashMap<UUID, JobDTO> concurrentHashMap) {

        this.concurrentHashMap = concurrentHashMap;
    }

    @Override
    public void run() {
        System.out.println("ConfigReaderJob.run() - reading file - Important - This job is not removing lines from config file. I had problem with this and it need time to analyze problem.");
        //Read from file for new config
        try (Stream<String> stream = Files.lines(Paths.get(ClassLoader.getSystemResource("jobs.properties").toURI()))) {

            stream
                    .filter(line -> !line.startsWith("#"))
                    .filter(line -> !line.isEmpty())
                    .map(line -> line.split(";"))
                    .map(jobLineSplitted -> new JobDTO(jobLineSplitted[0].trim(), jobLineSplitted[1].trim()))
                    .forEach(jobDTO -> {
                        System.out.println("Inputting "+jobDTO);
                        concurrentHashMap.put(UUID.randomUUID(), jobDTO);
                    });

        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }

     }
}
