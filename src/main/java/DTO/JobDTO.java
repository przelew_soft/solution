package DTO;

/**
 * Created by przelew on 2017-06-18.
 */
public final class JobDTO {

    private final String name;
    private final String dateToRun;
    private final boolean startNow;


    public JobDTO(String name, String dateToRun) {
        this.name = name;
        this.dateToRun = dateToRun;

        if (dateToRun.equals("NOW"))
        {
            this.startNow = true;
        }
        else
        {
            this.startNow = false;
        }
    }

    public String getName() {
        return name;
    }

    public String getDateToRun() {
        return dateToRun;
    }

    public boolean isStartNow() {
        return startNow;
    }

    @Override
    public String toString() {
        return "DTO.JobDTO{" +
                "name='" + name + '\'' +
                ", dateToRun='" + dateToRun + '\'' +
                ", startNow=" + startNow +
                '}';
    }
}
